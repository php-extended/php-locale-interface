<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-locale-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Locale;

use Stringable;

/**
 * LocaleInterface interface file.
 * 
 * An locale represents a language identifier with its scripting specificities.
 * 
 * Examples of identifiers include:
 * - en-US (English, United States)
 * - zh-Hant-TW (Chinese, Traditional Script, Taiwan)
 * - fr-CA, fr-FR (French for Canada and France respectively)
 * 
 * @author Anastaszor
 */
interface LocaleInterface extends Stringable
{
	
	/**
	 * Gets the name of the locale.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the primary language for the input locale.
	 * 
	 * @return string
	 */
	public function getPrimaryLanguage() : string;
	
	/**
	 * Gets the script for this locale.
	 * 
	 * @return ?string
	 */
	public function getScript() : ?string;
	
	/**
	 * Gets the region for this locale.
	 * 
	 * @return ?string
	 */
	public function getRegion() : ?string;
	
	/**
	 * Gets the keywords for this locale.
	 * 
	 * @return array<integer, string>
	 */
	public function getKeywords() : array;
	
	/**
	 * Returns an appropriately localized display name for script of the input
	 * locale.
	 * 
	 * @param LocaleInterface $inLocale
	 * @return string
	 */
	public function getDisplayScript(LocaleInterface $inLocale) : string;
	
	/**
	 * Returns an appropriately localized display name for region of the input
	 * locale.
	 * 
	 * @param LocaleInterface $inLocale
	 * @return ?string
	 */
	public function getDisplayRegion(LocaleInterface $inLocale) : ?string;
	
	/**
	 * Returns an appropriately localized display name for the input locale. 
	 * 
	 * @param LocaleInterface $inLocale
	 * @return ?string
	 */
	public function getDisplayName(LocaleInterface $inLocale) : ?string;
	
	/**
	 * Returns an appropriately localized display name for language of the
	 * input locale.
	 * 
	 * @param LocaleInterface $inLocale
	 * @return ?string
	 */
	public function getDisplayLanguage(LocaleInterface $inLocale) : ?string;
	
	/**
	 * Returns an appropriately localized display name for variants of the
	 * input locale.
	 * 
	 * @param LocaleInterface $inLocale
	 * @return ?string
	 */
	public function getDisplayVariant(LocaleInterface $inLocale) : ?string;
	
	/**
	 * Gets the variants for this locale.
	 * 
	 * @return array<integer, string>
	 */
	public function getAllVariants() : array;
	
	/**
	 * Gets whether this locale equals the other given object.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets a string representation of this locale suitable for the http header
	 * Accept-Language.
	 * 
	 * @return string
	 */
	public function toAcceptHttpHeader() : string;
	
}
